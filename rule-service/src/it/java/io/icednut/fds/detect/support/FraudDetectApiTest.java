package io.icednut.fds.detect.support;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.icednut.fds.engine.model.FraudDetectResultContent;
import io.icednut.fdsx.AbstractIntegrationTests;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 10.
 */
public class FraudDetectApiTest extends AbstractIntegrationTests {

    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * RuleA가 맞는 경우
     *
     * @throws Exception
     */
    @Test
    public void checkFraud_RuleA_is_ok() throws Exception {
        String userId = "11";
        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "ACCOUNT_CREATE")
                .param("accountNumber", "11-11")).andExpect(status().isOk());

        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "ADD_MONEY")
                .param("addingMoneyAmount", "200000")
                .param("bankAccountAmount", "0")).andExpect(status().isOk());

        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "SEND_MONEY")
                .param("sendAccountNumber", "22-241-22")
                .param("sendAccountBeforeMoneyAmount", "200000")
                .param("receiveUserId", "22")
                .param("sendMoneyAmount", "199000")).andExpect(status().isOk());

        MvcResult response = mockMvc.perform(get("/v1/fraud/" + userId))
                .andExpect(status().isOk())
                .andReturn();

        String responseJson = response.getResponse().getContentAsString();
        FraudDetectResultContent fraudDetectResultContent = objectMapper.readValue(responseJson, FraudDetectResultContent.class);

        assertThat(fraudDetectResultContent.getUserId(), equalTo(Long.parseLong(userId)));
        assertThat(fraudDetectResultContent.getIsFraud(), equalTo(true));
        assertThat(fraudDetectResultContent.getRule(), containsString("RuleA"));
    }

    /**
     * RuleB가 맞는 경우
     */
    @Test
    public void checkFraud_RuleB_is_ok() throws Exception {
        String userId = "11";
        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "ACCOUNT_CREATE")
                .param("accountNumber", "11-11")).andExpect(status().isOk());

        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "RECEIVE_MONEY")
                .param("receiveAccountNumber", "11-11")
                .param("receiveAccountBeforeMoneyAmount", "0")
                .param("sendingAccountNumber", "22-22")
                .param("sendingUserId", "12")
                .param("receiveMoneyAmount", "50000")
        ).andExpect(status().isOk());

        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "RECEIVE_MONEY")
                .param("receiveAccountNumber", "11-12")
                .param("receiveAccountBeforeMoneyAmount", "0")
                .param("sendingAccountNumber", "22-22")
                .param("sendingUserId", "12")
                .param("receiveMoneyAmount", "100000")
        ).andExpect(status().isOk());

        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "RECEIVE_MONEY")
                .param("receiveAccountNumber", "11-11")
                .param("receiveAccountBeforeMoneyAmount", "0")
                .param("sendingAccountNumber", "22-22")
                .param("sendingUserId", "13")
                .param("receiveMoneyAmount", "100000")
        ).andExpect(status().isOk());

        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "RECEIVE_MONEY")
                .param("receiveAccountNumber", "11-11")
                .param("receiveAccountBeforeMoneyAmount", "100000")
                .param("sendingAccountNumber", "22-22")
                .param("sendingUserId", "15")
                .param("receiveMoneyAmount", "1000000")
        ).andExpect(status().isOk());

        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "RECEIVE_MONEY")
                .param("receiveAccountNumber", "11-11")
                .param("receiveAccountBeforeMoneyAmount", "1100000")
                .param("sendingAccountNumber", "22-22")
                .param("sendingUserId", "15")
                .param("receiveMoneyAmount", "1000000")
        ).andExpect(status().isOk());

        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "RECEIVE_MONEY")
                .param("receiveAccountNumber", "11-11")
                .param("receiveAccountBeforeMoneyAmount", "2100000")
                .param("sendingAccountNumber", "22-22")
                .param("sendingUserId", "15")
                .param("receiveMoneyAmount", "100000")
        ).andExpect(status().isOk());

        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "RECEIVE_MONEY")
                .param("receiveAccountNumber", "11-11")
                .param("receiveAccountBeforeMoneyAmount", "2200000")
                .param("sendingAccountNumber", "22-22")
                .param("sendingUserId", "15")
                .param("receiveMoneyAmount", "100000")
        ).andExpect(status().isOk());

        MvcResult response = mockMvc.perform(get("/v1/fraud/" + userId))
                .andExpect(status().isOk())
                .andReturn();

        String responseJson = response.getResponse().getContentAsString();
        FraudDetectResultContent fraudDetectResultContent = objectMapper.readValue(responseJson, FraudDetectResultContent.class);

        assertThat(fraudDetectResultContent.getUserId(), equalTo(Long.parseLong(userId)));
        assertThat(fraudDetectResultContent.getIsFraud(), equalTo(true));
        assertThat(fraudDetectResultContent.getRule(), containsString("RuleB"));
    }

    /**
     * RuleC가 맞는 경우
     */
    @Test
    public void checkFraud_RuleC_is_ok() throws Exception {
        String userId = "11";
        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "ACCOUNT_CREATE")
                .param("accountNumber", "11-11")).andExpect(status().isOk());

        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "RECEIVE_MONEY")
                .param("receiveAccountNumber", "11-11")
                .param("receiveAccountBeforeMoneyAmount", "0")
                .param("sendingAccountNumber", "22-22")
                .param("sendingUserId", "12")
                .param("receiveMoneyAmount", "50000")
        ).andExpect(status().isOk());

        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "RECEIVE_MONEY")
                .param("receiveAccountNumber", "11-11")
                .param("receiveAccountBeforeMoneyAmount", "50000")
                .param("sendingAccountNumber", "22-22")
                .param("sendingUserId", "12")
                .param("receiveMoneyAmount", "50000")
        ).andExpect(status().isOk());

        mockMvc.perform(post("/v1/trade-log")
                .param("userId", userId)
                .param("logType", "RECEIVE_MONEY")
                .param("receiveAccountNumber", "11-11")
                .param("receiveAccountBeforeMoneyAmount", "100000")
                .param("sendingAccountNumber", "22-22")
                .param("sendingUserId", "12")
                .param("receiveMoneyAmount", "50000")
        ).andExpect(status().isOk());

        MvcResult response = mockMvc.perform(get("/v1/fraud/" + userId))
                .andExpect(status().isOk())
                .andReturn();

        String responseJson = response.getResponse().getContentAsString();
        FraudDetectResultContent fraudDetectResultContent = objectMapper.readValue(responseJson, FraudDetectResultContent.class);

        assertThat(fraudDetectResultContent.getUserId(), equalTo(Long.parseLong(userId)));
        assertThat(fraudDetectResultContent.getIsFraud(), equalTo(true));
        assertThat(fraudDetectResultContent.getRule(), containsString("RuleC"));
    }
}
