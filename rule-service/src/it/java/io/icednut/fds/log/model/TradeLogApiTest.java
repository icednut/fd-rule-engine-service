package io.icednut.fds.log.model;

import io.icednut.fdsx.AbstractIntegrationTests;
import org.junit.Test;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
public class TradeLogApiTest extends AbstractIntegrationTests {

    @Test
    public void checkFraud_ok() throws Exception {
        MockHttpServletRequestBuilder request = post("/v1/trade-log")
                .param("userId", "11")
                .param("logType", "ACCOUNT_CREATE")
                .param("accountNumber", "11-11");

        mockMvc.perform(request).andExpect(status().isOk());
    }
}
