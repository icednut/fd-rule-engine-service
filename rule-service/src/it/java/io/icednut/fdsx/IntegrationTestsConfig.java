package io.icednut.fdsx;

import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Mockito.mock;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 10.
 */
@Configuration
class IntegrationTestsConfig {

    @Primary
    @Bean
    public RestTemplate mockRestTemplate() {
        return mock(RestTemplate.class);
    }

    @Bean
    public MockMvcBuilderCustomizer mockMvcBuilderCustomizer(ListableBeanFactory beanFactory) {
//        Filter springSecurityFilterChain = beanFactory.getBean("springSecurityFilterChain", Filter.class);
//        return builder -> builder.addFilters(springSecurityFilterChain)
//                .defaultRequest(get("/").with(csrf()).with(user(123L).roles("ADMIN")))
//                .build();
//        Filter springSecurityFilterChain = beanFactory.getBean("springSecurityFilterChain", Filter.class);
        return builder -> builder.build();
    }
}
