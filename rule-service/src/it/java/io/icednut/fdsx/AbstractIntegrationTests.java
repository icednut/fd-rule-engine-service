package io.icednut.fdsx;

import io.icednut.fds.FdsRuleEngineApplication;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 10.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
@ContextConfiguration(classes = {IntegrationTestsConfig.class, FdsRuleEngineApplication.class})
@Transactional
@AutoConfigureMockMvc(addFilters = false)
public abstract class AbstractIntegrationTests {

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    protected MockMvc mockMvc;

//    @Autowired
//    private CacheManager cacheManager;
//
//    @Before
//    public void setupMockMvc() {
//    }
//
//    @After
//    public void clearCaches() throws Exception {
//        for (String name : cacheManager.getCacheNames()) {
//            cacheManager.getCache(name).clear();
//        }
//    }
}
