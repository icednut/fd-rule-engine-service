package io.icednut.fds.detect;

import io.icednut.fds.detect.FraudDetectController;
import io.icednut.fds.engine.model.FraudDetectResultContent;
import io.icednut.fds.detect.service.FraudDetectService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.Mockito.*;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 9.
 */
@RunWith(MockitoJUnitRunner.class)
public class FraudDetectControllerTest {

    private FraudDetectController controller;

    @Mock
    private FraudDetectService service;

    @Before
    public void setUp() {
        controller = new FraudDetectController(service);
    }

    @Test
    public void checkFraud() {
        long userId = 11L;
        when(service.checkFraud(any())).thenReturn(returnValue(userId));

        ResponseEntity<FraudDetectResultContent> response = controller.checkFraud(userId);

        assertThat(response.getBody(), not(nullValue()));
        assertThat(response.getBody().getUserId(), equalTo(userId));
        assertThat(response.getBody().getIsFraud(), equalTo(false));
        verify(service, times(1)).checkFraud(userId);
    }

    private FraudDetectResultContent returnValue(long userId) {
        return new FraudDetectResultContent(userId, false, null);
    }
}