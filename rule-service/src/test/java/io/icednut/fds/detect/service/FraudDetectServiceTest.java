package io.icednut.fds.detect.service;

import io.icednut.fds.cache.entity.TradeLogFact;
import io.icednut.fds.cache.repository.TradeLogFactRepository;
import io.icednut.fds.engine.model.FraudDetectResultContent;
import io.icednut.fds.engine.RuleEngine;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
@RunWith(MockitoJUnitRunner.class)
public class FraudDetectServiceTest {

    private FraudDetectService service;

    @Mock
    private TradeLogFactRepository repository;

    @Mock
    private RuleEngine ruleEngine;

    @Before
    public void setUp() {
        this.service = new FraudDetectService(repository, ruleEngine);
    }

    @Test
    public void checkFraud() {
        Long userId = 1L;
        Optional<TradeLogFact> maybeTradeLogFact = Optional.empty();
        when(repository.findById(userId)).thenReturn(maybeTradeLogFact);
        when(ruleEngine.runRuleCheck(userId, maybeTradeLogFact)).thenReturn(new FraudDetectResultContent(userId, false, ""));

        FraudDetectResultContent detectResult = service.checkFraud(userId);

        assertThat(detectResult, not(nullValue()));
        verify(repository, times(1)).findById(userId);
        verify(ruleEngine, times(1)).runRuleCheck(userId, maybeTradeLogFact);
    }
}