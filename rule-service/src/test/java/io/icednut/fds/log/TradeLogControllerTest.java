package io.icednut.fds.log;

import io.icednut.fds.db.type.LogType;
import io.icednut.fds.log.TradeLogController;
import io.icednut.fds.log.model.TradeLogParam;
import io.icednut.fds.log.service.TradeLogService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
@RunWith(MockitoJUnitRunner.class)
public class TradeLogControllerTest {

    private TradeLogController controller;

    @Mock
    private TradeLogService service;

    @Before
    public void setUp() {
        controller = new TradeLogController(service);
    }

    @Test
    public void saveTradeLog() {
        TradeLogParam param = givenParam();

        ResponseEntity<String> response = controller.saveTradeLog(param);

        assertThat(response.getBody(), equalTo("success"));
        verify(service, times(1)).saveTradeLog(param);
    }

    private TradeLogParam givenParam() {
        return new TradeLogParam(
                1L, LogType.ACCOUNT_CREATE,
                "11-11",
                null, null, null, null, null, null, null, null, null, null, null
        );
    }
}