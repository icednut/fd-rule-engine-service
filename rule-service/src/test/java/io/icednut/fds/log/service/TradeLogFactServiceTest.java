package io.icednut.fds.log.service;

import io.icednut.fds.cache.entity.TradeLogFact;
import io.icednut.fds.cache.repository.TradeLogFactRepository;
import io.icednut.fds.db.entity.TradeLog;
import io.icednut.fds.db.type.LogType;
import io.icednut.fds.log.service.fact.TradeLogFactGenerator;
import io.icednut.fds.log.service.fact.TradeLogFactService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
@RunWith(MockitoJUnitRunner.class)
public class TradeLogFactServiceTest {

    private TradeLogFactService service;

    @Mock
    private TradeLogFactRepository repository;

    @Mock
    private TradeLogFactGenerator tradeLogFactGenerator;

    @Before
    public void setUp() {
        HashMap<LogType, TradeLogFactGenerator> tradeLogFactGeneratorMap = new HashMap<>();
        tradeLogFactGeneratorMap.put(LogType.ACCOUNT_CREATE, tradeLogFactGenerator);

        this.service = new TradeLogFactService(repository, tradeLogFactGeneratorMap);
    }

    @Test
    public void updateTradeTraceFact() {
        TradeLog tradeLog = TradeLog.create()
                .userId(1L)
                .logType(LogType.ACCOUNT_CREATE)
                .build();
        when(tradeLogFactGenerator.generate(tradeLog)).thenReturn(Optional.of(new TradeLogFact()));

        service.updateTradeTraceFact(tradeLog);

        verify(repository, times(1)).save(any(TradeLogFact.class));
    }
}