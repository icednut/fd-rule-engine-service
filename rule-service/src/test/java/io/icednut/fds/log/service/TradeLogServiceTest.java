package io.icednut.fds.log.service;

import io.icednut.fds.db.type.LogType;
import io.icednut.fds.db.repository.TradeLogRepository;
import io.icednut.fds.db.entity.TradeLog;
import io.icednut.fds.log.model.TradeLogParam;
import io.icednut.fds.log.service.fact.TradeLogFactService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
@RunWith(MockitoJUnitRunner.class)
public class TradeLogServiceTest {

    private TradeLogService service;

    @Mock
    private TradeLogRepository tradeLogRepository;

    @Mock
    private TradeLogFactService tradeLogFactService;

    @Before
    public void setUp() {
        this.service = new TradeLogService(tradeLogRepository, tradeLogFactService);
    }

    @Test
    public void saveTradeLog() {
        TradeLogParam param = givenParam();
        when(tradeLogRepository.save(any(TradeLog.class))).thenReturn(param.toEntity());

        this.service.saveTradeLog(param);

        verify(tradeLogRepository, times(1)).save(any(TradeLog.class));
        verify(tradeLogFactService, times(1)).updateTradeTraceFact(any(TradeLog.class));
    }

    private TradeLogParam givenParam() {
        return new TradeLogParam(
                1L, LogType.ACCOUNT_CREATE,
                "11-11",
                null, null, null, null, null, null, null, null, null, null, null
        );
    }
}