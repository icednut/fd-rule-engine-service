package io.icednut.fds.config;

import io.icednut.fds.cache.entity.TradeLogFact;
import org.apache.geode.cache.client.ClientRegionShortcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.gemfire.config.annotation.ClientCacheApplication;
import org.springframework.data.gemfire.config.annotation.EnableEntityDefinedRegions;
import org.springframework.data.gemfire.repository.config.EnableGemfireRepositories;
import org.springframework.data.gemfire.transaction.config.EnableGemfireCacheTransactions;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
@Configuration
@ClientCacheApplication(name = "AccessingDataGemFireApplication", logLevel = "error")
@EnableEntityDefinedRegions(basePackageClasses = {TradeLogFact.class}, clientRegionShortcut = ClientRegionShortcut.LOCAL)
@EnableGemfireRepositories({"io.icednut.fds.cache.repository"})
@EnableGemfireCacheTransactions
public class CacheConfig {
}
