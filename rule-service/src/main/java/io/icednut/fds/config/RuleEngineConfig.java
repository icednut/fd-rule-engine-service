package io.icednut.fds.config;

import io.icednut.fds.engine.RuleEngine;
import io.icednut.fds.engine.rule.RuleBuilder;
import io.icednut.fds.engine.rule.predict.RulePredictor;
import io.icednut.fds.engine.rule.predict.RulePredictorA;
import io.icednut.fds.engine.rule.predict.RulePredictorB;
import io.icednut.fds.engine.rule.predict.RulePredictorC;
import io.icednut.fds.engine.rule.result.DefaultResultGenerator;
import io.icednut.fds.engine.rule.result.RuleResultGenerator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
@Configuration
public class RuleEngineConfig {

    @Bean
    public RulePredictor rulePredictorA() {
        return new RulePredictorA();
    }

    @Bean
    public RulePredictor rulePredictorB() {
        return new RulePredictorB();
    }

    @Bean
    public RulePredictor rulePredictorC() {
        return new RulePredictorC();
    }

    @Bean
    public RuleResultGenerator defaultResultHandler() {
        return new DefaultResultGenerator();
    }

    @Bean
    public RuleEngine ruleEngine(
            RuleResultGenerator defaultResultHandler,
            @Qualifier("rulePredictorA") RulePredictor rulePredictorA,
            @Qualifier("rulePredictorB") RulePredictor rulePredictorB,
            @Qualifier("rulePredictorC") RulePredictor rulePredictorC
    ) {
        RuleEngine ruleEngine = new RuleEngine();

        // RuleA: 카카오머니 서비스 계좌 개설을 하고 1시간 이내, 20만원 충전 후 잔액이 1000원 이하가 되는 경우
        ruleEngine.addRule(
                RuleBuilder
                        .create()
                        .ruleName("RuleA")
                        .when(rulePredictorA::is)
                        .then(defaultResultHandler::result)
                        .build()
        );

        // RuleB: 카카오머니 서비스 계좌 개설을 하고7일 이내, 카카오머니 받기로 10만원 이상 금액을 5회 이상 하는 경우
        ruleEngine.addRule(
                RuleBuilder
                        .create()
                        .ruleName("RuleB")
                        .when(rulePredictorB::is)
                        .then(defaultResultHandler::result)
                        .build()
        );

        // RuleC: 2시간 이내, 카카오머니 받기로 5만원 이상 금액을 3회 이상 하는 경우
        ruleEngine.addRule(
                RuleBuilder
                        .create()
                        .ruleName("RuleC")
                        .when(rulePredictorC::is)
                        .then(defaultResultHandler::result)
                        .build()
        );
        return ruleEngine;
    }
}
