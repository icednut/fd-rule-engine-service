package io.icednut.fds.config;

import io.icednut.fds.cache.repository.TradeLogFactRepository;
import io.icednut.fds.db.type.LogType;
import io.icednut.fds.log.service.fact.TradeLogFactGenerator;
import io.icednut.fds.log.service.fact.strategy.AccountCreateLogFactGenerator;
import io.icednut.fds.log.service.fact.strategy.AddMoneyLogFactGenerator;
import io.icednut.fds.log.service.fact.strategy.ReceiveMoneyLogFactGenerator;
import io.icednut.fds.log.service.fact.strategy.SendMoneyLogFactGenerator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 8.
 */
@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Bean
    public TradeLogFactGenerator accoutCreateLogFactGenerator() {
        return new AccountCreateLogFactGenerator();
    }

    @Bean
    public TradeLogFactGenerator addMoneyLogFactGenerator(TradeLogFactRepository repository) {
        return new AddMoneyLogFactGenerator(repository);
    }

    @Bean
    public TradeLogFactGenerator receiveMoneyLogFactGenerator(TradeLogFactRepository repository) {
        return new ReceiveMoneyLogFactGenerator(repository);
    }

    @Bean
    public TradeLogFactGenerator sendMoneyLogFactGenerator(TradeLogFactRepository repository) {
        return new SendMoneyLogFactGenerator(repository);
    }

    @Bean
    public Map<LogType, TradeLogFactGenerator> tradeLogFactGeneratorMap(
            @Qualifier("accoutCreateLogFactGenerator") TradeLogFactGenerator accoutCreateLogFactGenerator,
            @Qualifier("addMoneyLogFactGenerator") TradeLogFactGenerator addMoneyLogFactGenerator,
            @Qualifier("receiveMoneyLogFactGenerator") TradeLogFactGenerator receiveMoneyLogFactGenerator,
            @Qualifier("sendMoneyLogFactGenerator") TradeLogFactGenerator sendMoneyLogFactGenerator
    ) {
        HashMap<LogType, TradeLogFactGenerator> tradeLogFactGeneratorHashMap = new HashMap<>();

        tradeLogFactGeneratorHashMap.put(LogType.ACCOUNT_CREATE, accoutCreateLogFactGenerator);
        tradeLogFactGeneratorHashMap.put(LogType.ADD_MONEY, addMoneyLogFactGenerator);
        tradeLogFactGeneratorHashMap.put(LogType.RECEIVE_MONEY, receiveMoneyLogFactGenerator);
        tradeLogFactGeneratorHashMap.put(LogType.SEND_MONEY, sendMoneyLogFactGenerator);
        return tradeLogFactGeneratorHashMap;
    }
}
