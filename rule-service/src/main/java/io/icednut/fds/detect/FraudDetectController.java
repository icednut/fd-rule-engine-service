package io.icednut.fds.detect;

import io.icednut.fds.detect.service.FraudDetectService;
import io.icednut.fds.engine.model.FraudDetectResultContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 8.
 */
@RestController
@RequestMapping("/v1/fraud")
public class FraudDetectController {

    private final FraudDetectService service;

    @Autowired
    public FraudDetectController(FraudDetectService service) {
        this.service = service;
    }

    /**
     * 부정거래 로그 검사 API
     *
     * @param userId
     * @return
     */
    @GetMapping("/{user_id}")
    public ResponseEntity<FraudDetectResultContent> checkFraud(@PathVariable("user_id") Long userId) {
        FraudDetectResultContent result = service.checkFraud(userId);
        return ResponseEntity.ok(result);
    }
}
