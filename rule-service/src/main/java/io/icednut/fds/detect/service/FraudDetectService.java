package io.icednut.fds.detect.service;

import io.icednut.fds.cache.entity.TradeLogFact;
import io.icednut.fds.cache.repository.TradeLogFactRepository;
import io.icednut.fds.engine.model.FraudDetectResultContent;
import io.icednut.fds.engine.RuleEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 9.
 */
@Service
public class FraudDetectService {

    private final TradeLogFactRepository repository;

    private final RuleEngine ruleEngine;

    @Autowired
    public FraudDetectService(TradeLogFactRepository repository, RuleEngine ruleEngine) {
        this.repository = repository;
        this.ruleEngine = ruleEngine;
    }

    @Transactional(readOnly = true)
    public FraudDetectResultContent checkFraud(Long userId) {
        Optional<TradeLogFact> maybeTradeLogFact = repository.findById(userId);

        return ruleEngine.runRuleCheck(userId, maybeTradeLogFact);
    }
}
