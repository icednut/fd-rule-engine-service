package io.icednut.fds.log.service.fact;

import io.icednut.fds.cache.entity.TradeLogFact;
import io.icednut.fds.db.entity.TradeLog;

import java.util.Optional;

/**
 * @author wangeun.lee@sk.com
 * @created 2018. 3. 13.
 */
public interface TradeLogFactGenerator {

    Optional<TradeLogFact> generate(TradeLog tradeLog);
}
