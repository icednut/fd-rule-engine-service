package io.icednut.fds.log.service.fact.strategy;

import io.icednut.fds.cache.entity.TradeLogFact;
import io.icednut.fds.cache.repository.TradeLogFactRepository;
import io.icednut.fds.db.entity.TradeLog;
import io.icednut.fds.log.service.fact.TradeLogFactGenerator;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public class AddMoneyLogFactGenerator implements TradeLogFactGenerator {

    private final TradeLogFactRepository repository;

    @Autowired
    public AddMoneyLogFactGenerator(TradeLogFactRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<TradeLogFact> generate(TradeLog tradeLog) {
        Long userId = tradeLog.getUserId();
        Optional<TradeLogFact> maybeTradeTraceFact = repository.findById(userId);

        maybeTradeTraceFact.ifPresent(tradeLogFact -> {
            tradeLogFact.pushAddMoneyEvent(
                    new TradeLogFact.AddMoneyEventFact(tradeLog.getAddingMoneyAmount(), tradeLog.getCreatedDate())
            );
            tradeLogFact.pushLastBalanceFact(
                    new TradeLogFact.LastBalanceFact(tradeLog.getBankAccountAmount() + tradeLog.getAddingMoneyAmount(), tradeLog.getCreatedDate())
            );
        });
        return maybeTradeTraceFact;
    }
}
