package io.icednut.fds.log.service;

import io.icednut.fds.db.repository.TradeLogRepository;
import io.icednut.fds.db.entity.TradeLog;
import io.icednut.fds.log.model.TradeLogParam;
import io.icednut.fds.log.service.fact.TradeLogFactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
@Service
public class TradeLogService {

    private final TradeLogRepository tradeLogRepository;
    private final TradeLogFactService tradeLogFactService;

    @Autowired
    public TradeLogService(TradeLogRepository tradeLogRepository, TradeLogFactService tradeLogFactService) {
        this.tradeLogRepository = tradeLogRepository;
        this.tradeLogFactService = tradeLogFactService;
    }

    @Transactional
    public void saveTradeLog(TradeLogParam param) {
        TradeLog tradeLog = saveTradeLogInternal(param);
        tradeLogFactService.updateTradeTraceFact(tradeLog);
    }

    private TradeLog saveTradeLogInternal(TradeLogParam param) {
        TradeLog tradeLog = param.toEntity();

        return tradeLogRepository.save(tradeLog);
    }
}
