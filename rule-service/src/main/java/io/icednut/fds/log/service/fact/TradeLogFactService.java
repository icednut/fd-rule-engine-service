package io.icednut.fds.log.service.fact;

import io.icednut.fds.cache.entity.TradeLogFact;
import io.icednut.fds.cache.repository.TradeLogFactRepository;
import io.icednut.fds.db.entity.TradeLog;
import io.icednut.fds.db.type.LogType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Optional;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
@Service
public class TradeLogFactService {

    private final TradeLogFactRepository repository;
    private final Map<LogType, TradeLogFactGenerator> tradeLogFactGeneratorMap;

    @Autowired
    public TradeLogFactService(TradeLogFactRepository repository, Map<LogType, TradeLogFactGenerator> tradeLogFactGeneratorMap) {
        this.repository = repository;
        this.tradeLogFactGeneratorMap = tradeLogFactGeneratorMap;
    }

    @Transactional
    public void updateTradeTraceFact(TradeLog tradeLog) {
        LogType logType = tradeLog.getLogType();
        Optional<TradeLogFactGenerator> maybeTradeLogFactGenerator = Optional.ofNullable(tradeLogFactGeneratorMap.get(logType));

        if (!maybeTradeLogFactGenerator.isPresent()) {
            return;
        }

        TradeLogFactGenerator tradeLogFactGenerator = maybeTradeLogFactGenerator.get();
        Optional<TradeLogFact> maybeTradeTraceFact = tradeLogFactGenerator.generate(tradeLog);

        maybeTradeTraceFact.ifPresent(tradeLogFact -> {
            repository.save(tradeLogFact);
        });
    }
}
