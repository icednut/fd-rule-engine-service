package io.icednut.fds.log.service.fact.strategy;

import io.icednut.fds.cache.entity.TradeLogFact;
import io.icednut.fds.cache.repository.TradeLogFactRepository;
import io.icednut.fds.db.entity.TradeLog;
import io.icednut.fds.log.service.fact.TradeLogFactGenerator;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public class ReceiveMoneyLogFactGenerator implements TradeLogFactGenerator {

    private final TradeLogFactRepository repository;

    @Autowired
    public ReceiveMoneyLogFactGenerator(TradeLogFactRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<TradeLogFact> generate(TradeLog tradeLog) {
        Long userId = tradeLog.getUserId();
        Optional<TradeLogFact> maybeTradeTraceFact = repository.findById(userId);

        maybeTradeTraceFact.ifPresent(tradeLogFact -> {
            tradeLogFact.pushReceiveMoneyEvent(
                    new TradeLogFact.ReceiveEventFact(tradeLog.getReceiveMoneyAmount(), tradeLog.getReceiveAccountNumber(), tradeLog.getCreatedDate())
            );
            tradeLogFact.pushLastBalanceFact(
                    new TradeLogFact.LastBalanceFact(tradeLog.getReceiveAccountBeforeMoneyAmount() + tradeLog.getReceiveMoneyAmount(), tradeLog.getCreatedDate())
            );
        });

        return maybeTradeTraceFact;
    }
}
