package io.icednut.fds.log.model;

import io.icednut.fds.db.entity.TradeLog;
import io.icednut.fds.db.type.LogType;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
public class TradeLogParam {

    private Long userId;
    private LogType logType;

    // ACCOUNT_CREATE
    private String accountNumber;

    // SEND_MONEY
    private String sendAccountNumber;
    private Long sendAccountBeforeMoneyAmount;
    private Long receiveUserId;
    private Long sendMoneyAmount;

    // ADD_MONEY
    private Long addingMoneyAmount;
    private Long bankAccountAmount;

    // RECEIVE_MONEY
    private String receiveAccountNumber;
    private Long receiveAccountBeforeMoneyAmount;
    private String sendingAccountNumber;
    private Long sendingUserId;
    private Long receiveMoneyAmount;

    public TradeLogParam(Long userId, LogType logType, String accountNumber, String sendAccountNumber, Long sendAccountBeforeMoneyAmount, Long receiveUserId, Long sendMoneyAmount, Long addingMoneyAmount, Long bankAccountAmount, String receiveAccountNumber, Long receiveAccountBeforeMoneyAmount, String sendingAccountNumber, Long sendingUserId, Long receiveMoneyAmount) {
        this.userId = userId;
        this.logType = logType;
        this.accountNumber = accountNumber;
        this.sendAccountNumber = sendAccountNumber;
        this.sendAccountBeforeMoneyAmount = sendAccountBeforeMoneyAmount;
        this.receiveUserId = receiveUserId;
        this.sendMoneyAmount = sendMoneyAmount;
        this.addingMoneyAmount = addingMoneyAmount;
        this.bankAccountAmount = bankAccountAmount;
        this.receiveAccountNumber = receiveAccountNumber;
        this.receiveAccountBeforeMoneyAmount = receiveAccountBeforeMoneyAmount;
        this.sendingAccountNumber = sendingAccountNumber;
        this.sendingUserId = sendingUserId;
        this.receiveMoneyAmount = receiveMoneyAmount;
    }

    public static Builder crete() {
        return new Builder();
    }

    public Long getUserId() {
        return userId;
    }

    public LogType getLogType() {
        return logType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getSendAccountNumber() {
        return sendAccountNumber;
    }

    public Long getSendAccountBeforeMoneyAmount() {
        return sendAccountBeforeMoneyAmount;
    }

    public Long getReceiveUserId() {
        return receiveUserId;
    }

    public Long getSendMoneyAmount() {
        return sendMoneyAmount;
    }

    public Long getAddingMoneyAmount() {
        return addingMoneyAmount;
    }

    public Long getBankAccountAmount() {
        return bankAccountAmount;
    }

    public String getReceiveAccountNumber() {
        return receiveAccountNumber;
    }

    public Long getReceiveAccountBeforeMoneyAmount() {
        return receiveAccountBeforeMoneyAmount;
    }

    public String getSendingAccountNumber() {
        return sendingAccountNumber;
    }

    public Long getSendingUserId() {
        return sendingUserId;
    }

    public Long getReceiveMoneyAmount() {
        return receiveMoneyAmount;
    }

    public TradeLog toEntity() {
        return TradeLog
                .create()
                .userId(userId)
                .logType(logType)
                .accountNumber(accountNumber)
                .sendAccountNumber(sendAccountNumber)
                .sendAccountBeforeMoneyAmount(sendAccountBeforeMoneyAmount)
                .receiveUserId(receiveUserId)
                .sendMoneyAmount(sendMoneyAmount)
                .addingMoneyAmount(addingMoneyAmount)
                .bankAccountAmount(bankAccountAmount)
                .receiveAccountNumber(receiveAccountNumber)
                .receiveAccountBeforeMoneyAmount(receiveAccountBeforeMoneyAmount)
                .sendingAccountNumber(sendingAccountNumber)
                .sendingUserId(sendingUserId)
                .receiveMoneyAmount(receiveMoneyAmount)
                .build();
    }

    public static class Builder {
        private Long userId;
        private LogType logType;

        // ACCOUNT_CREATE
        private String accountNumber;

        // SEND_MONEY
        private String sendAccountNumber;
        private Long sendAccountBeforeMoneyAmount;
        private Long receiveUserId;
        private Long sendMoneyAmount;

        // ADD_MONEY
        private Long addingMoneyAmount;
        private Long bankAccountAmount;

        // RECEIVE_MONEY
        private String receiveAccountNumber;
        private Long receiveAccountBeforeMoneyAmount;
        private String sendingAccountNumber;
        private Long sendingUserId;
        private Long receiveMoneyAmount;

        public TradeLogParam build() {
            return new TradeLogParam(
                    userId,
                    logType,
                    accountNumber,
                    sendAccountNumber,
                    sendAccountBeforeMoneyAmount,
                    receiveUserId,
                    sendMoneyAmount,
                    addingMoneyAmount,
                    bankAccountAmount,
                    receiveAccountNumber,
                    receiveAccountBeforeMoneyAmount,
                    sendingAccountNumber,
                    sendingUserId,
                    receiveMoneyAmount);
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder logType(LogType logType) {
            this.logType = logType;
            return this;
        }

        public Builder accountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        public Builder addingMoneyAmount(Long addingMoneyAmount) {
            this.addingMoneyAmount = addingMoneyAmount;
            return this;
        }

        public Builder bankAccountAmount(Long bankAccountAmount) {
            this.bankAccountAmount = bankAccountAmount;
            return this;
        }

        public Builder sendAccountNumber(String sendAccountNumber) {
            this.sendAccountNumber = sendAccountNumber;
            return this;
        }

        public Builder sendAccountBeforeMoneyAmount(Long sendAccountBeforeMoneyAmount) {
            this.sendAccountBeforeMoneyAmount = sendAccountBeforeMoneyAmount;
            return this;
        }

        public Builder receiveUserId(Long receiveUserId) {
            this.receiveUserId = receiveUserId;
            return this;
        }

        public Builder sendMoneyAmount(Long sendMoneyAmount) {
            this.sendMoneyAmount = sendMoneyAmount;
            return this;
        }

        public Builder receiveAccountNumber(String receiveAccountNumber) {
            this.receiveAccountNumber = receiveAccountNumber;
            return this;
        }

        public Builder receiveAccountBeforeMoneyAmount(Long receiveAccountBeforeMoneyAmount) {
            this.receiveAccountBeforeMoneyAmount = receiveAccountBeforeMoneyAmount;
            return this;
        }

        public Builder sendingAccountNumber(String sendingAccountNumber) {
            this.sendingAccountNumber = sendingAccountNumber;
            return this;
        }

        public Builder sendingUserId(Long sendingUserId) {
            this.sendingUserId = sendingUserId;
            return this;
        }

        public Builder receiveMoneyAmount(Long receiveMoneyAmount) {
            this.receiveMoneyAmount = receiveMoneyAmount;
            return this;
        }
    }
}
