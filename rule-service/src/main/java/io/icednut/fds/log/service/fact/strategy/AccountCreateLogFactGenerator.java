package io.icednut.fds.log.service.fact.strategy;

import io.icednut.fds.cache.entity.TradeLogFact;
import io.icednut.fds.db.entity.TradeLog;
import io.icednut.fds.log.service.fact.TradeLogFactGenerator;

import java.util.Optional;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public class AccountCreateLogFactGenerator implements TradeLogFactGenerator {

    @Override
    public Optional<TradeLogFact> generate(TradeLog tradeLog) {
        Long userId = tradeLog.getUserId();
        TradeLogFact traceFact = new TradeLogFact();

        traceFact.setUserId(userId);
        traceFact.setAccountCreateTime(tradeLog.getCreatedDate());
        traceFact.setCreatedAccountNumber(tradeLog.getAccountNumber());
        return Optional.of(traceFact);
    }
}
