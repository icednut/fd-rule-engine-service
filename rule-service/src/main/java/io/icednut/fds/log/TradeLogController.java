package io.icednut.fds.log;

import io.icednut.fds.log.model.TradeLogParam;
import io.icednut.fds.log.service.TradeLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
@RestController
@RequestMapping("/v1/trade-log")
public class TradeLogController {

    private final TradeLogService service;

    @Autowired
    public TradeLogController(TradeLogService service) {
        this.service = service;
    }

    /**
     * 사용자 로그 저장 API
     *
     * @param param
     * @return
     */
    @PostMapping
    public ResponseEntity<String> saveTradeLog(TradeLogParam param) {
        service.saveTradeLog(param);
        return ResponseEntity.ok("success");
    }
}
