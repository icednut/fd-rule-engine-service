package io.icednut.fds;

import io.icednut.fds.db.type.LogType;
import io.icednut.fds.log.model.TradeLogParam;
import io.icednut.fds.log.service.TradeLogService;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
public class FdsRuleEngineApplication {

    public static void main(String[] args) {
        SpringApplication.run(FdsRuleEngineApplication.class, args);
    }

    @Bean
    @Profile("mockup")
    ApplicationRunner run(TradeLogService tradeLogService) {
        return args -> {
            Long userId = 1L;

            tradeLogService.saveTradeLog(
                    TradeLogParam.crete()
                            .userId(userId)
                            .logType(LogType.ACCOUNT_CREATE)
                            .accountNumber("11-11")
                            .build()
            );
            tradeLogService.saveTradeLog(
                    TradeLogParam.crete()
                            .userId(userId)
                            .logType(LogType.ADD_MONEY)
                            .addingMoneyAmount(200000L)
                            .bankAccountAmount(0L)
                            .build()
            );
            tradeLogService.saveTradeLog(
                    TradeLogParam.crete()
                            .userId(userId)
                            .logType(LogType.SEND_MONEY)
                            .sendAccountNumber("22-241-22")
                            .sendAccountBeforeMoneyAmount(200000L)
                            .receiveUserId(22L)
                            .sendMoneyAmount(199000L)
                            .build()
            );
            tradeLogService.saveTradeLog(
                    TradeLogParam.crete()
                            .userId(userId)
                            .logType(LogType.RECEIVE_MONEY)
                            .receiveAccountNumber("11-11")
                            .receiveAccountBeforeMoneyAmount(1000L)
                            .sendingAccountNumber("22-22")
                            .sendingUserId(2L)
                            .receiveMoneyAmount(100000L)
                            .build()
            );
            tradeLogService.saveTradeLog(
                    TradeLogParam.crete()
                            .userId(userId)
                            .logType(LogType.RECEIVE_MONEY)
                            .receiveAccountNumber("11-11")
                            .receiveAccountBeforeMoneyAmount(101000L)
                            .sendingAccountNumber("22-22")
                            .sendingUserId(2L)
                            .receiveMoneyAmount(100000L)
                            .build()
            );
            tradeLogService.saveTradeLog(
                    TradeLogParam.crete()
                            .userId(userId)
                            .logType(LogType.RECEIVE_MONEY)
                            .receiveAccountNumber("11-11")
                            .receiveAccountBeforeMoneyAmount(201000L)
                            .sendingAccountNumber("22-22")
                            .sendingUserId(2L)
                            .receiveMoneyAmount(100000L)
                            .build()
            );
            tradeLogService.saveTradeLog(
                    TradeLogParam.crete()
                            .userId(userId)
                            .logType(LogType.RECEIVE_MONEY)
                            .receiveAccountNumber("11-11")
                            .receiveAccountBeforeMoneyAmount(301000L)
                            .sendingAccountNumber("22-22")
                            .sendingUserId(2L)
                            .receiveMoneyAmount(100000L)
                            .build()
            );
            tradeLogService.saveTradeLog(
                    TradeLogParam.crete()
                            .userId(userId)
                            .logType(LogType.RECEIVE_MONEY)
                            .receiveAccountNumber("11-11")
                            .receiveAccountBeforeMoneyAmount(401000L)
                            .sendingAccountNumber("22-22")
                            .sendingUserId(2L)
                            .receiveMoneyAmount(100000L)
                            .build()
            );
            System.out.println("*** Mock data init complete!!!");
        };
    }
}
