# Fraud Detect System
해당 프로젝트는 부정거래 판별 RESTful Service 입니다. (이하 fd-system으로 호칭) 사용자 로그를 해당 시스템을 통해 입수한 뒤 userId를 기준으로 특정 부정거래 룰(Rule)에 해당 되는지를 Restful API를 통해 판별하는 시스템입니다.
fd-system의 프로젝트 구조는 다음과 같습니다.

```
.
├── README.md
├── build.gradle
├── *entity
│   ├── build.gradle
│   └── src
│       └── main
│           └── java
├── gradle
├── *rule-engine
│   ├── build.gradle
│   └── src
│       ├── main
│       │   └── java
│       └── test
│           └── java
├── *rule-service
│   ├── build.gradle
│   └── src
│       ├── it
│       │   ├── java
│       │   └── resources
│       ├── main
│       │   ├── java
│       │   └── resources
│       └── test
│           └── java
└── settings.gradle
```

- **entity**: fd-system에서 사용하는 Database, Cache 접근을 위한 엔티티 클래스 프로젝트
- **rule-engine**: 거래로그에서 특정 Rule 패턴 룰에 매칭되는지 판별하는 룰 엔진 프로젝트
- **rule-service**: rule-engine을 기반으로 사용자 부정거래 판별 Restful API 서비스 프로젝트

## Project Spec
- Gradle 4.1
- Spring Boot 2.0.0.RELEASE
- Spring Data JPA
- JPA 2.1 (with Hibernate)
- H2 Database
- Pivotal Gemfire

자세한 디펜던시는 아래 명령어로 확인할 수 있습니다.
```
$ ./gradlew :entity:dependencies
$ ./gradlew :rule-engine:dependencies
$ ./gradlew :rule-service:dependencies
```

## Build
소스코드를 받으신 후 터미널을 오픈하여 아래 명령어를 실행하면 빌드가 진행됩니다.
```
$ git clone https://gitlab.com/icednut/fd-rule-engine-service.git
$ cd fd-rule-engine-service
$ ./gradlew build
```

## Test
프로젝트 구성 시 유닛테스트(Unit Test)와 통합테스트(Integration Test) 코드를 디렉토리 레벨(src/test/java, src/it/java)로 분리하였습니다.
따라서 아래 명령어를 실행하면 유닛테스트와 통합테스트가 같이 실행되게 됩니다.
```
$ ./gradlew test
```

통합테스트만 실행하고 싶을 경우 아래 명령어를 사용합니다.
```
$ ./gradlew integTest
```

## Run
실행은 아래 명령어를 통해 실행합니다.
```
$ ./gradlew bootRun
```
사용자 로그에 대한 Mock-up 데이터를 미리 준비 받은 뒤 실행하고 싶다면 아래 명령어를 통해 profile을 설정하여 실행합니다.
```
$ SPRING_PROFILES_ACTIVE=mockup ./gradlew bootRun
``` 
위 명령어로 실행하면 user_id = 1에 대한 사용자 로그가 Database와 Cache에 미리 올라가게 됩니다.
```
$ curl -X GET http://localhost:8080/v1/fraud/1
{"userId":1,"isFraud":true,"rule":"RuleA,RuleB,RuleC"}
```

## rule-service에 구성된 API
#### 1. 로그 입수
##### API 기본 정보
| 메서드 | 요청 URL | 출력포맷 |
| ----  | ------- | ----- |
| POST | /v1/trade-log | JSON |

##### 요청 변수
| 요청 변수 | 타입 | 필수유무 | 설명 |
| ------  | --- | ------ | --- |
| userId | long | Y | 사용자ID | 
| logType | string | Y | 로그 타입 (ACCOUNT_CREATE, SEND_MONEY, ADD_MONEY, RECEIVE_MONEY) | 
| accountNumber | string |  | 카카오머니 계좌번호 | 
| sendAccountNumber | string |  | 송금 카카오머니 계좌번호 | 
| sendAccountNumber | string |  | 송금 카카오머니 계좌번호 | 
| sendAccountBeforeMoneyAmount | long |  | 송금 이체전 카카오머니 계좌 잔액 | 
| receiveAccountNumber | string |  | 수신 카카오머니 계좌번호 | 
| receiveUserId | long |  | 수신 사용자 ID | 
| sendMoneyAmount | long |  | 송금 금액 | 
| addingMoneyAmount | long |  | 충전 금액 | 
| bankAccountAmount | long |  | 은행 계좌 잔액 (은행 계좌에서 머니 계좌로 이체) | 
| receiveAccountBeforeMoneyAmount | long |  | 받기 전 카카오머니 계좌 잔액 | 
| sendingAccountNumber | string |  | 보낸 카카오머니 계좌번호 | 
| sendingUserId | long |  | 보낸 사용자 ID | 
| receiveMoneyAmount | long |  | 받기금액 | 

#### 2. 부정거래 대상자 판별 
##### API 기본 정보
| 메서드 | 요청 URL | 출력포맷 |
| ----  | ------- | ----- |
| POST | /v1/fraud/{user_id} | JSON |

##### 요청 변수
| 요청 변수 | 타입 | 필수유무 | 설명 |
| ------  | --- | ------ | --- |
| user_id | long | Y | 사용자ID |

##### 출력 결과
| 필드 | 타입 | 설명 |
| ----  | ------- | ----- |
| user_id | long | 요청 변수의 사용자 ID |
| is_fraud | boolean | 룰에 의한 이상 거래 가능성 여부 |
| rule | string | 해당되는 룰 이름 (여러개 일 경우는 여러개 표기(콤마로 구분)) |
 

## 코드 설명
### 1. 로그 입수
로그 입수 API(/v1/trade-log)를 통해 로그를 한 줄 입력받게 되면 먼저 RDBMS(H2, MySQL)로 저장 합니다.
그 후 Gemfire 캐시에 해당 사용자의 거래기록이력(TradeLogFact)을 저장합니다.
거래기록이력 저장 시 Gemfire의 트랜잭션 처리를 지원 받습니다.
캐시에 저장된 거래기록 이력은 추후 룰엔진(rule-engine)에서 사용 합니다.

```java
@Service
public class TradeLogService {

    private final TradeLogRepository tradeLogRepository;
    private final TradeLogFactService tradeLogFactService;

    @Autowired
    public TradeLogService(TradeLogRepository tradeLogRepository, TradeLogFactService tradeLogFactService) {
        this.tradeLogRepository = tradeLogRepository;
        this.tradeLogFactService = tradeLogFactService;
    }

    @Transactional
    public void saveTradeLog(TradeLogParam param) {
        TradeLog tradeLog = saveTradeLogInternal(param);
        tradeLogFactService.updateTradeTraceFact(tradeLog);
    }

    ...
}
```

```java
@Service
public class TradeLogFactService {

    private final TradeLogFactRepository repository;
    private final Map<LogType, TradeLogFactGenerator> tradeLogFactGeneratorMap;

    @Autowired
    public TradeLogFactService(TradeLogFactRepository repository, Map<LogType, TradeLogFactGenerator> tradeLogFactGeneratorMap) {
        this.repository = repository;
        this.tradeLogFactGeneratorMap = tradeLogFactGeneratorMap;
    }

    @Transactional
    public void updateTradeTraceFact(TradeLog tradeLog) {
        LogType logType = tradeLog.getLogType();
        Optional<TradeLogFactGenerator> maybeTradeLogFactGenerator = Optional.ofNullable(tradeLogFactGeneratorMap.get(logType));

        if (!maybeTradeLogFactGenerator.isPresent()) {
            return;
        }

        TradeLogFactGenerator tradeLogFactGenerator = maybeTradeLogFactGenerator.get();
        Optional<TradeLogFact> maybeTradeTraceFact = tradeLogFactGenerator.generate(tradeLog);

        maybeTradeTraceFact.ifPresent(tradeLogFact -> {
            repository.save(tradeLogFact);
        });
    }
}
```
전략 패턴(Strategy Pattern)을 사용하여 입수되는 거래로그(TradeLog) 마다 거래로그이력을 어떻게 생성하고 append 할지 나뉘어져 있습니다.

### 2. 부정거래 판별
부정거래 대상자 판별 API(/v1/fraud/{user_id})를 요청을 받게 되면 해당 사용자의 거래기록이력(TradeLogFact)를 조회하여 룰 엔진(RuleEngine)을 통해 부정거래 판별을 진행합니다.

```java
@Service
public class FraudDetectService {

    private final TradeLogFactRepository repository;

    private final RuleEngine ruleEngine;

    @Autowired
    public FraudDetectService(TradeLogFactRepository repository, RuleEngine ruleEngine) {
        this.repository = repository;
        this.ruleEngine = ruleEngine;
    }

    @Transactional(readOnly = true)
    public FraudDetectResultContent checkFraud(Long userId) {
        Optional<TradeLogFact> maybeTradeLogFact = repository.findById(userId);

        return ruleEngine.runRuleCheck(userId, maybeTradeLogFact);
    }
}
```

룰 엔진은 Chain of Responsibility 패턴을 사용하여 룰(Rule)을 자유자재로 추가하고 실행할 수 있습니다.
또한 Thread-safe를 고려하여 RuleEngine은 상태를 갖고 있지 않으며 Rule 검증(ruleHandler.handle() 메소드)을 실행할 때마다 결과를 새로 생성합니다.
```java
public class RuleEngine {

    private RuleExecuteHandler<FraudDetectResultContent> headHandler;
    private RuleExecuteHandler<FraudDetectResultContent> tailHandler;

    public void addRule(Rule rule) {
        if (rule == null) {
            return;
        }

        RuleExecuteHandler<FraudDetectResultContent> ruleHandler = new RuleExecuteHandler<>(rule);

        if (headHandler == null) {
            headHandler = ruleHandler;
            tailHandler = ruleHandler;
        } else {
            tailHandler = tailHandler.nextHandler(ruleHandler);
        }
    }

    public FraudDetectResultContent runRuleCheck(Long userId, Optional<TradeLogFact> maybeTradeLogFact) {
        if (!maybeTradeLogFact.isPresent()) {
            return new FraudDetectResultContent(userId, false, "");
        }

        Fact fact = maybeTradeLogFact.get();
        RuleDecisionResult<FraudDetectResultContent> result = new RuleDecisionResult<>(new FraudDetectResultContent(userId));

        Optional.ofNullable(this.headHandler)
                .ifPresent(ruleHandler -> ruleHandler.handle(fact, result));

        return result.getContent();
    }
}
```

룰 엔진의 핵심인 룰(Rule)은 내부적으로 룰 검증을 담당하는 when과 룰 매칭 시 결과를 처리하는 then이라는 로직으로 구성되어 있으며, when과 then은 확장에는 오픈될 수 있도록 유연한 구조로 되어 있습니다.
```java
public class Rule<T extends Fact> {

    private final String ruleName;
    private final RulePredictor<T> when;
    private final RuleResultGenerator then;

    public Rule(String ruleName, RulePredictor<T> when, RuleResultGenerator then) {
        this.ruleName = ruleName;
        this.when = when;
        this.then = then;
    }

    public ResultContent check(T fact, ResultContent previousResult) {
        if (when.is(fact) == false) {
            return previousResult;
        }
        return then.result(previousResult, ruleName);
    }
}
```

룰 검증을 담당하는 when의 RulePredictor 하나를 살펴 보면 거래기록이력 오브젝트만 사용하기 때문에 추가로 DB나 Cache에서 데이터를 불러오지 않습니다.
따라서 룰 검증에 관련된 로직만 구성되어 있어서 유닛테스트에도 용이합니다.
```java
public class RulePredictorA implements RulePredictor<TradeLogFact> {

    /**
     * RuleA: 카카오머니 서비스 계좌 개설을 하고 1시간 이내, 20만원 충전 후 잔액이 1000원 이하가 되는 경우
     *
     * @param fact
     * @return
     */
    @Override
    public boolean is(TradeLogFact fact) {
        LocalDateTime accountCreateTime = fact.getAccountCreateTime();
        boolean addMoneyEventOccur = fact.isAddMoneyEventOccur();

        if (!addMoneyEventOccur) {
            return false;
        }
        LocalDateTime oneHourAfter = accountCreateTime.plusHours(1);

        Optional<TradeLogFact.AddMoneyEventFact> firstCondition = Optional.ofNullable(fact.getAddMoneyEvents()).orElse(Collections.emptyList())
                .stream()
                .filter(addMoneyEventFact -> addMoneyEventFact.getCreatedTime().isBefore(oneHourAfter))
                .filter(addMoneyEventFact -> addMoneyEventFact.getAddMoneyAmount() >= 200000L)
                .findFirst();

        if (!firstCondition.isPresent()) {
            return false;
        }

        Optional<TradeLogFact.LastBalanceFact> secondCondition = Optional.ofNullable(fact.getLastBalanceFacts()).orElse(Collections.emptyList())
                .stream()
                .filter(lastBalanceFact -> {
                    LocalDateTime createdTime = lastBalanceFact.getCreatedTime();

                    return createdTime.isAfter(firstCondition.get().getCreatedTime()) && createdTime.isBefore(oneHourAfter);
                })
                .filter(lastBalanceFact -> lastBalanceFact.getLastBalance() <= 1000L)
                .findFirst();

        if (!secondCondition.isPresent()) {
            return false;
        }
        return true;
    }
}
```

이상 입니다.
end of document