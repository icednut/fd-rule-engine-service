package io.icednut.fds.cache.entity;

import io.icednut.fds.cache.type.Fact;
import org.springframework.data.annotation.Id;
import org.springframework.data.gemfire.mapping.annotation.Region;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
@Region
public class TradeLogFact implements Serializable, Fact {

    @Id
    private Long userId;
    private LocalDateTime accountCreateTime;
    private String createdAccountNumber;

    private boolean addMoneyEventOccur;
    private List<AddMoneyEventFact> addMoneyEvents;

    private boolean receiveMoneyEventOccur;
    private List<ReceiveEventFact> receiveMoneyEvents;

    private boolean sendMoneyEventOccur;
    private List<SendMoneyEventFact> sendMoneyEvents;

    private List<LastBalanceFact> lastBalanceFacts;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void pushReceiveMoneyEvent(ReceiveEventFact receiveEventFact) {
        if (receiveMoneyEvents == null) {
            this.receiveMoneyEvents = new ArrayList<>();
        }
        this.receiveMoneyEvents.add(receiveEventFact);
        this.receiveMoneyEventOccur = true;
    }

    public void pushAddMoneyEvent(AddMoneyEventFact addMoneyEventFact) {
        if (addMoneyEvents == null) {
            this.addMoneyEvents = new ArrayList<>();
        }
        this.addMoneyEvents.add(addMoneyEventFact);
        this.addMoneyEventOccur = true;
    }

    public void pushSendMoneyEvent(SendMoneyEventFact sendMoneyEventFact) {
        if (this.sendMoneyEvents == null) {
            this.sendMoneyEvents = new ArrayList<>();
        }
        sendMoneyEvents.add(sendMoneyEventFact);
        this.sendMoneyEventOccur = true;
    }

    public void pushLastBalanceFact(LastBalanceFact lastBalanceFact) {
        if (lastBalanceFacts == null) {
            this.lastBalanceFacts = new ArrayList<>();
        }
        lastBalanceFacts.add(lastBalanceFact);
    }

    public LocalDateTime getAccountCreateTime() {
        return accountCreateTime;
    }

    public void setAccountCreateTime(LocalDateTime accountCreateTime) {
        this.accountCreateTime = accountCreateTime;
    }

    public boolean isAddMoneyEventOccur() {
        return addMoneyEventOccur;
    }

    public boolean isReceiveMoneyEventOccur() {
        return receiveMoneyEventOccur;
    }

    public boolean isSendMoneyEventOccur() {
        return sendMoneyEventOccur;
    }

    public List<LastBalanceFact> getLastBalanceFacts() {
        return lastBalanceFacts;
    }

    public List<AddMoneyEventFact> getAddMoneyEvents() {
        return addMoneyEvents;
    }

    public List<ReceiveEventFact> getReceiveMoneyEvents() {
        return receiveMoneyEvents;
    }

    public List<SendMoneyEventFact> getSendMoneyEvents() {
        return sendMoneyEvents;
    }

    public String getCreatedAccountNumber() {
        return createdAccountNumber;
    }

    public void setCreatedAccountNumber(String createdAccountNumber) {
        this.createdAccountNumber = createdAccountNumber;
    }

    public static class ReceiveEventFact implements Serializable {
        private Long receiveMoneyAmount;
        private String receiveAccountNumber;
        private LocalDateTime lastReceiveMoneyEventOccurTime;

        public ReceiveEventFact(Long receiveMoneyAmount, String receiveAccountNumber, LocalDateTime lastReceiveMoneyEventOccurTime) {
            this.receiveMoneyAmount = receiveMoneyAmount;
            this.receiveAccountNumber = receiveAccountNumber;
            this.lastReceiveMoneyEventOccurTime = lastReceiveMoneyEventOccurTime;
        }

        public Long getReceiveMoneyAmount() {
            return receiveMoneyAmount;
        }

        public String getReceiveAccountNumber() {
            return receiveAccountNumber;
        }

        public LocalDateTime getLastReceiveMoneyEventOccurTime() {
            return lastReceiveMoneyEventOccurTime;
        }
    }

    public static class SendMoneyEventFact implements Serializable {
        private Long sendMoneyAmount;
        private LocalDateTime createdTime;

        public SendMoneyEventFact(Long sendMoneyAmount, LocalDateTime createdTime) {
            this.sendMoneyAmount = sendMoneyAmount;
            this.createdTime = createdTime;
        }

        public Long getSendMoneyAmount() {
            return sendMoneyAmount;
        }

        public LocalDateTime getCreatedTime() {
            return createdTime;
        }
    }

    public static class LastBalanceFact implements Serializable {
        private Long lastBalance;
        private LocalDateTime createdTime;

        public LastBalanceFact(Long lastBalance, LocalDateTime createdTime) {
            this.lastBalance = lastBalance;
            this.createdTime = createdTime;
        }

        public Long getLastBalance() {
            return lastBalance;
        }

        public LocalDateTime getCreatedTime() {
            return createdTime;
        }
    }

    public static class AddMoneyEventFact implements Serializable {
        private Long addMoneyAmount;
        private LocalDateTime createdTime;

        public AddMoneyEventFact(Long addMoneyAmount, LocalDateTime createdTime) {
            this.addMoneyAmount = addMoneyAmount;
            this.createdTime = createdTime;
        }

        public Long getAddMoneyAmount() {
            return addMoneyAmount;
        }

        public LocalDateTime getCreatedTime() {
            return createdTime;
        }
    }
}
