package io.icednut.fds.cache.repository;

import io.icednut.fds.cache.entity.TradeLogFact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
public interface TradeLogFactRepository extends CrudRepository<TradeLogFact, Long> {
}
