package io.icednut.fds.db.entity;

import io.icednut.fds.db.type.LogType;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
@Entity
@Table(name = "trade_log")
public class TradeLog extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long logId;

    private Long userId;
    private LogType logType;

    // ACCOUNT_CREATE
    private String accountNumber;

    // SEND_MONEY
    private String sendAccountNumber;
    private Long sendAccountBeforeMoneyAmount;
    private Long receiveUserId;
    private Long sendMoneyAmount;

    // ADD_MONEY
    private Long addingMoneyAmount;
    private Long bankAccountAmount;

    // RECEIVE_MONEY
    private String receiveAccountNumber;
    private Long receiveAccountBeforeMoneyAmount;
    private String sendingAccountNumber;
    private Long sendingUserId;
    private Long receiveMoneyAmount;

    public TradeLog() {
    }

    public static TradeLogBuilder create() {
        return new TradeLogBuilder();
    }

    public Long getLogId() {
        return logId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LogType getLogType() {
        return logType;
    }

    public void setLogType(LogType logType) {
        this.logType = logType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getSendAccountNumber() {
        return sendAccountNumber;
    }

    public void setSendAccountNumber(String sendAccountNumber) {
        this.sendAccountNumber = sendAccountNumber;
    }

    public Long getSendAccountBeforeMoneyAmount() {
        return sendAccountBeforeMoneyAmount;
    }

    public void setSendAccountBeforeMoneyAmount(Long sendAccountBeforeMoneyAmount) {
        this.sendAccountBeforeMoneyAmount = sendAccountBeforeMoneyAmount;
    }

    public Long getReceiveUserId() {
        return receiveUserId;
    }

    public void setReceiveUserId(Long receiveUserId) {
        this.receiveUserId = receiveUserId;
    }

    public Long getSendMoneyAmount() {
        return sendMoneyAmount;
    }

    public void setSendMoneyAmount(Long sendMoneyAmount) {
        this.sendMoneyAmount = sendMoneyAmount;
    }

    public Long getAddingMoneyAmount() {
        return addingMoneyAmount;
    }

    public void setAddingMoneyAmount(Long addingMoneyAmount) {
        this.addingMoneyAmount = addingMoneyAmount;
    }

    public Long getBankAccountAmount() {
        return bankAccountAmount;
    }

    public void setBankAccountAmount(Long bankAccountAmount) {
        this.bankAccountAmount = bankAccountAmount;
    }

    public String getReceiveAccountNumber() {
        return receiveAccountNumber;
    }

    public void setReceiveAccountNumber(String receiveAccountNumber) {
        this.receiveAccountNumber = receiveAccountNumber;
    }

    public Long getReceiveAccountBeforeMoneyAmount() {
        return receiveAccountBeforeMoneyAmount;
    }

    public void setReceiveAccountBeforeMoneyAmount(Long receiveAccountBeforeMoneyAmount) {
        this.receiveAccountBeforeMoneyAmount = receiveAccountBeforeMoneyAmount;
    }

    public String getSendingAccountNumber() {
        return sendingAccountNumber;
    }

    public void setSendingAccountNumber(String sendingAccountNumber) {
        this.sendingAccountNumber = sendingAccountNumber;
    }

    public Long getSendingUserId() {
        return sendingUserId;
    }

    public void setSendingUserId(Long sendingUserId) {
        this.sendingUserId = sendingUserId;
    }

    public Long getReceiveMoneyAmount() {
        return receiveMoneyAmount;
    }

    public void setReceiveMoneyAmount(Long receiveMoneyAmount) {
        this.receiveMoneyAmount = receiveMoneyAmount;
    }

    public static class TradeLogBuilder {
        private final TradeLog tradeLog;

        public TradeLogBuilder() {
            this.tradeLog = new TradeLog();
        }

        public TradeLog build() {
            return tradeLog;
        }

        public TradeLogBuilder userId(Long userId) {
            this.tradeLog.setUserId(userId);
            return this;
        }

        public TradeLogBuilder logType(LogType logType) {
            this.tradeLog.setLogType(logType);
            return this;
        }

        public TradeLogBuilder accountNumber(String accountNumber) {
            this.tradeLog.setAccountNumber(accountNumber);
            return this;
        }

        public TradeLogBuilder sendAccountNumber(String sendAccountNumber) {
            this.tradeLog.setSendAccountNumber(sendAccountNumber);
            return this;
        }

        public TradeLogBuilder sendAccountBeforeMoneyAmount(Long sendAccountBeforeMoneyAmount) {
            this.tradeLog.setSendAccountBeforeMoneyAmount(sendAccountBeforeMoneyAmount);
            return this;
        }

        public TradeLogBuilder receiveUserId(Long receiveUserId) {
            this.tradeLog.setReceiveUserId(receiveUserId);
            return this;
        }

        public TradeLogBuilder sendMoneyAmount(Long sendMoneyAmount) {
            this.tradeLog.setSendMoneyAmount(sendMoneyAmount);
            return this;
        }

        public TradeLogBuilder addingMoneyAmount(Long addingMoneyAmount) {
            this.tradeLog.setAddingMoneyAmount(addingMoneyAmount);
            return this;
        }

        public TradeLogBuilder bankAccountAmount(Long bankAccountAmount) {
            this.tradeLog.setBankAccountAmount(bankAccountAmount);
            return this;
        }

        public TradeLogBuilder receiveAccountNumber(String receiveAccountNumber) {
            this.tradeLog.setReceiveAccountNumber(receiveAccountNumber);
            return this;
        }

        public TradeLogBuilder receiveAccountBeforeMoneyAmount(Long receiveAccountBeforeMoneyAmount) {
            this.tradeLog.setReceiveAccountBeforeMoneyAmount(receiveAccountBeforeMoneyAmount);
            return this;
        }

        public TradeLogBuilder sendingAccountNumber(String sendingAccountNumber) {
            this.tradeLog.setSendingAccountNumber(sendingAccountNumber);
            return this;
        }

        public TradeLogBuilder sendingUserId(Long sendingUserId) {
            this.tradeLog.setSendingUserId(sendingUserId);
            return this;
        }

        public TradeLogBuilder receiveMoneyAmount(Long receiveMoneyAmount) {
            this.tradeLog.setReceiveMoneyAmount(receiveMoneyAmount);
            return this;
        }
    }
}
