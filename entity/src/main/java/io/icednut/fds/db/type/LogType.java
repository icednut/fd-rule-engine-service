package io.icednut.fds.db.type;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
public enum LogType {
    ACCOUNT_CREATE, SEND_MONEY, ADD_MONEY, RECEIVE_MONEY
}
