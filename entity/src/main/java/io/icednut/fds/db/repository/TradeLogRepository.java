package io.icednut.fds.db.repository;

import io.icednut.fds.db.entity.TradeLog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 12.
 */
public interface TradeLogRepository extends JpaRepository<TradeLog, Long> {
}
