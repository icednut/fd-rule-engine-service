package io.icednut.fds.engine.rule.predict;

import io.icednut.fds.cache.entity.TradeLogFact;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public class RulePredictorC implements RulePredictor<TradeLogFact> {

    /**
     * RuleC: 2시간 이내, 카카오머니 받기로 5만원 이상 금액을 3회 이상 하는 경우
     *
     * @param fact
     * @return
     */
    @Override
    public boolean is(TradeLogFact fact) {
        String createdAccountNumber = fact.getCreatedAccountNumber();

        List<TradeLogFact.ReceiveEventFact> targetEvents = Optional.ofNullable(fact.getReceiveMoneyEvents()).orElse(Collections.emptyList())
                .stream()
                .filter(receiveEventFact -> {
                    if (!receiveEventFact.getReceiveAccountNumber().equals(createdAccountNumber)) {
                        return false;
                    }

                    return receiveEventFact.getReceiveMoneyAmount() >= 50000;
                })
                .collect(Collectors.toList());

        if (targetEvents.size() < 3) {
            return false;
        }

        for (int i = 1; i < targetEvents.size() - 1; i++) {
            TradeLogFact.ReceiveEventFact frontTargetEvent = targetEvents.get(i - 1);
            TradeLogFact.ReceiveEventFact rearTargetEvent = targetEvents.get(i + 1);

            LocalDateTime frontEventTime = frontTargetEvent.getLastReceiveMoneyEventOccurTime();
            LocalDateTime rearEventTime = rearTargetEvent.getLastReceiveMoneyEventOccurTime();

            Duration between = Duration.between(frontEventTime, rearEventTime);

            if (between.getSeconds() <= 60 * 60 * 2) {
                return true;
            }
        }
        return false;
    }
}
