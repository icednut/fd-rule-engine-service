package io.icednut.fds.engine.rule;

import io.icednut.fds.cache.type.Fact;
import io.icednut.fds.engine.model.ResultContent;
import io.icednut.fds.engine.model.RuleDecisionResult;

import java.util.Optional;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public class RuleExecuteHandler<T extends ResultContent> {

    private Rule rule;
    private RuleExecuteHandler<T> ruleHandler;

    public RuleExecuteHandler(Rule rule) {
        this.rule = rule;
    }

    public RuleExecuteHandler<T> nextHandler(RuleExecuteHandler<T> ruleHandler) {
        if (ruleHandler != null) {
            this.ruleHandler = ruleHandler;
        }
        return ruleHandler;
    }

    public void handle(Fact fact, RuleDecisionResult<T> result) {
        ResultContent resultContent = rule.check(fact, result.getContent());
        if (resultContent.isChanged()) {
            result.replaceContent(resultContent);
        }

        Optional.ofNullable(ruleHandler)
                .ifPresent((RuleExecuteHandler<T> nextRulekHandler) -> nextRulekHandler.handle(fact, result));
    }
}
