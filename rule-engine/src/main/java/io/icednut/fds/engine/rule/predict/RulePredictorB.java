package io.icednut.fds.engine.rule.predict;

import io.icednut.fds.cache.entity.TradeLogFact;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public class RulePredictorB implements RulePredictor<TradeLogFact> {

    /**
     * RuleB: 카카오머니 서비스 계좌 개설을 하고7일 이내, 카카오머니 받기로 10만원 이상 금액을 5회 이상 하는 경우
     *
     * @param fact
     * @return
     */
    @Override
    public boolean is(TradeLogFact fact) {
        LocalDateTime accountCreateTime = fact.getAccountCreateTime();
        String createdAccountNumber = fact.getCreatedAccountNumber();
        LocalDateTime sevenDaysAfter = accountCreateTime.plusDays(7);

        if (!fact.isReceiveMoneyEventOccur()) {
            return false;
        }

        long receiveCount = Optional.ofNullable(fact.getReceiveMoneyEvents()).orElse(Collections.emptyList())
                .stream()
                .filter(receiveEventFact -> receiveEventFact.getLastReceiveMoneyEventOccurTime().isBefore(sevenDaysAfter))
                .filter(receiveEventFact -> {
                    if (!receiveEventFact.getReceiveAccountNumber().equals(createdAccountNumber)) {
                        return false;
                    }

                    return receiveEventFact.getReceiveMoneyAmount() >= 100000;
                })
                .count();
        return receiveCount >= 5;
    }
}
