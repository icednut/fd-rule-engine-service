package io.icednut.fds.engine;

import io.icednut.fds.cache.entity.TradeLogFact;
import io.icednut.fds.cache.type.Fact;
import io.icednut.fds.engine.model.FraudDetectResultContent;
import io.icednut.fds.engine.model.RuleDecisionResult;
import io.icednut.fds.engine.rule.Rule;
import io.icednut.fds.engine.rule.RuleExecuteHandler;

import java.util.Optional;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public class RuleEngine {

    private RuleExecuteHandler<FraudDetectResultContent> headHandler;
    private RuleExecuteHandler<FraudDetectResultContent> tailHandler;

    public void addRule(Rule rule) {
        if (rule == null) {
            return;
        }

        RuleExecuteHandler<FraudDetectResultContent> ruleHandler = new RuleExecuteHandler<>(rule);

        if (headHandler == null) {
            headHandler = ruleHandler;
            tailHandler = ruleHandler;
        } else {
            tailHandler = tailHandler.nextHandler(ruleHandler);
        }
    }

    public FraudDetectResultContent runRuleCheck(Long userId, Optional<TradeLogFact> maybeTradeLogFact) {
        if (!maybeTradeLogFact.isPresent()) {
            return new FraudDetectResultContent(userId, false, "");
        }

        Fact fact = maybeTradeLogFact.get();
        RuleDecisionResult<FraudDetectResultContent> result = new RuleDecisionResult<>(new FraudDetectResultContent(userId));

        Optional.ofNullable(this.headHandler)
                .ifPresent(ruleHandler -> ruleHandler.handle(fact, result));

        return result.getContent();
    }
}
