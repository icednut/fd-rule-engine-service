package io.icednut.fds.engine.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public interface ResultContent {

    Long getUserId();

    @JsonIgnore
    boolean isChanged();

    @JsonIgnore
    String getContent();

}
