package io.icednut.fds.engine.rule.predict;

import io.icednut.fds.cache.entity.TradeLogFact;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public class RulePredictorA implements RulePredictor<TradeLogFact> {

    /**
     * RuleA: 카카오머니 서비스 계좌 개설을 하고 1시간 이내, 20만원 충전 후 잔액이 1000원 이하가 되는 경우
     *
     * @param fact
     * @return
     */
    @Override
    public boolean is(TradeLogFact fact) {
        LocalDateTime accountCreateTime = fact.getAccountCreateTime();
        boolean addMoneyEventOccur = fact.isAddMoneyEventOccur();

        if (!addMoneyEventOccur) {
            return false;
        }
        LocalDateTime oneHourAfter = accountCreateTime.plusHours(1);

        Optional<TradeLogFact.AddMoneyEventFact> firstCondition = Optional.ofNullable(fact.getAddMoneyEvents()).orElse(Collections.emptyList())
                .stream()
                .filter(addMoneyEventFact -> addMoneyEventFact.getCreatedTime().isBefore(oneHourAfter))
                .filter(addMoneyEventFact -> addMoneyEventFact.getAddMoneyAmount() >= 200000L)
                .findFirst();

        if (!firstCondition.isPresent()) {
            return false;
        }

        Optional<TradeLogFact.LastBalanceFact> secondCondition = Optional.ofNullable(fact.getLastBalanceFacts()).orElse(Collections.emptyList())
                .stream()
                .filter(lastBalanceFact -> {
                    LocalDateTime createdTime = lastBalanceFact.getCreatedTime();

                    return createdTime.isAfter(firstCondition.get().getCreatedTime()) && createdTime.isBefore(oneHourAfter);
                })
                .filter(lastBalanceFact -> lastBalanceFact.getLastBalance() <= 1000L)
                .findFirst();

        if (!secondCondition.isPresent()) {
            return false;
        }
        return true;
    }
}
