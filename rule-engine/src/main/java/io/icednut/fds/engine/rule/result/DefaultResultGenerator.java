package io.icednut.fds.engine.rule.result;

import io.icednut.fds.engine.model.FraudDetectResultContent;
import io.icednut.fds.engine.model.ResultContent;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public class DefaultResultGenerator implements RuleResultGenerator {

    public ResultContent result(ResultContent previousResult, String ruleName) {
        String previousRule = previousResult.getContent();
        String rule = (previousRule == null || previousRule.equals("")) ? ruleName : previousRule + "," + ruleName;

        return new FraudDetectResultContent(previousResult.getUserId(), true, rule);
    }
}
