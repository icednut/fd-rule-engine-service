package io.icednut.fds.engine.model;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 8.
 */
public class FraudDetectResultContent implements ResultContent {

    private final Long userId;
    private final Boolean isFraud;
    private final String rule;

    public FraudDetectResultContent() {
        this.userId = null;
        this.isFraud = false;
        this.rule = "";
    }

    public FraudDetectResultContent(Long userId) {
        this(userId, false, "");
    }

    public FraudDetectResultContent(Long userId, Boolean isFraud, String rule) {
        this.userId = userId;
        this.isFraud = isFraud;
        this.rule = rule;
    }

    @Override
    public Long getUserId() {
        return userId;
    }

    @Override
    public boolean isChanged() {
        return getIsFraud();
    }

    @Override
    public String getContent() {
        return getRule();
    }

    public Boolean getIsFraud() {
        return isFraud;
    }

    public String getRule() {
        return rule;
    }
}
