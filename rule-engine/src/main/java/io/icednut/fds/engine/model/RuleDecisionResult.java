package io.icednut.fds.engine.model;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public class RuleDecisionResult<T extends ResultContent> {

    private T content;

    public RuleDecisionResult(T content) {
        this.content = content;
    }

    public T getContent() {
        return content;
    }

    public void replaceContent(ResultContent resultContent) {
        content = (T) resultContent;
    }
}
