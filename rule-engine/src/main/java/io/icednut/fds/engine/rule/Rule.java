package io.icednut.fds.engine.rule;

import io.icednut.fds.cache.type.Fact;
import io.icednut.fds.engine.model.ResultContent;
import io.icednut.fds.engine.rule.predict.RulePredictor;
import io.icednut.fds.engine.rule.result.RuleResultGenerator;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public class Rule<T extends Fact> {

    private final String ruleName;
    private final RulePredictor<T> when;
    private final RuleResultGenerator then;

    public Rule(String ruleName, RulePredictor<T> when, RuleResultGenerator then) {
        this.ruleName = ruleName;
        this.when = when;
        this.then = then;
    }

    public ResultContent check(T fact, ResultContent previousResult) {
        if (when.is(fact) == false) {
            return previousResult;
        }
        return then.result(previousResult, ruleName);
    }
}
