package io.icednut.fds.engine.rule.result;

import io.icednut.fds.engine.model.ResultContent;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public interface RuleResultGenerator {
    ResultContent result(ResultContent previousResult, String ruleName);
}
