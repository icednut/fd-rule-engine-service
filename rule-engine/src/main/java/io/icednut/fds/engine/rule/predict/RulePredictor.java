package io.icednut.fds.engine.rule.predict;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public interface RulePredictor<T> {

    boolean is(T fact);
}
