package io.icednut.fds.engine.rule;

import io.icednut.fds.cache.type.Fact;
import io.icednut.fds.engine.rule.result.RuleResultGenerator;
import io.icednut.fds.engine.rule.predict.RulePredictor;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public class RuleBuilder {

    private String ruleName;
    private RulePredictor<? extends Fact> when;
    private RuleResultGenerator then;

    private RuleBuilder() {
    }

    public static RuleBuilder create() {
        return new RuleBuilder();
    }

    public RuleBuilder ruleName(String ruleName) {
        this.ruleName = ruleName;
        return this;
    }

    public RuleBuilder when(RulePredictor<? extends Fact> when) {
        this.when = when;
        return this;
    }

    public RuleBuilder then(RuleResultGenerator then) {
        this.then = then;
        return this;
    }

    public Rule build() {
        return new Rule(ruleName, when, then);
    }
}
