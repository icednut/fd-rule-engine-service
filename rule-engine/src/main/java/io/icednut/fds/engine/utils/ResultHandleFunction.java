package io.icednut.fds.engine.utils;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
@FunctionalInterface
public interface ResultHandleFunction<PARAM1, PARAM2, R> {

    R apply(PARAM1 p1, PARAM2 p2);

}
