package io.icednut.fds.engine;

import io.icednut.fds.cache.entity.TradeLogFact;
import io.icednut.fds.engine.model.FraudDetectResultContent;
import io.icednut.fds.engine.rule.RuleBuilder;
import io.icednut.fds.engine.rule.predict.RulePredictor;
import io.icednut.fds.engine.rule.predict.RulePredictorA;
import io.icednut.fds.engine.rule.predict.RulePredictorB;
import io.icednut.fds.engine.rule.predict.RulePredictorC;
import io.icednut.fds.engine.rule.result.DefaultResultGenerator;
import io.icednut.fds.engine.rule.result.RuleResultGenerator;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.IsNot.not;

/**
 * @author wglee21g@gmail.com
 * @created 2018. 3. 13.
 */
public class RuleEngineTest {

    private RuleEngine ruleEngine;

    @Before
    public void setUp() throws Exception {
        this.ruleEngine = new RuleEngine();
        RulePredictor rulePredictorA = new RulePredictorA();
        RulePredictor rulePredictorB = new RulePredictorB();
        RulePredictor rulePredictorC = new RulePredictorC();
        RuleResultGenerator defaultResultHandler = new DefaultResultGenerator();

        // RuleA: 카카오머니 서비스 계좌 개설을 하고 1시간 이내, 20만원 충전 후 잔액이 1000원 이하가 되는 경우
        ruleEngine.addRule(
                RuleBuilder
                        .create()
                        .ruleName("RuleA")
                        .when(rulePredictorA::is)
                        .then(defaultResultHandler::result)
                        .build()
        );

        // RuleB: 카카오머니 서비스 계좌 개설을 하고7일 이내, 카카오머니 받기로 10만원 이상 금액을 5회 이상 하는 경우
        ruleEngine.addRule(
                RuleBuilder
                        .create()
                        .ruleName("RuleB")
                        .when(rulePredictorB::is)
                        .then(defaultResultHandler::result)
                        .build()
        );

        // RuleC: 2시간 이내, 카카오머니 받기로 5만원 이상 금액을 3회 이상 하는 경우
        ruleEngine.addRule(
                RuleBuilder
                        .create()
                        .ruleName("RuleC")
                        .when(rulePredictorC::is)
                        .then(defaultResultHandler::result)
                        .build()
        );
    }

    @Test
    public void runRuleCheck_emptyFact() {
        Long userId = 1L;
        Optional<TradeLogFact> maybeTradeLogFact = Optional.empty();

        FraudDetectResultContent result = ruleEngine.runRuleCheck(userId, maybeTradeLogFact);

        assertThat(result.getUserId(), equalTo(userId));
        assertThat(result.getIsFraud(), equalTo(false));
    }

    @Test
    public void runRuleCheck_RuleA_is_ok() {
        Long userId = 1L;
        LocalDateTime now = LocalDateTime.now();
        TradeLogFact tradeLogFact = new TradeLogFact();
        tradeLogFact.setUserId(userId);
        tradeLogFact.setAccountCreateTime(now.minusHours(1));
        tradeLogFact.setCreatedAccountNumber("11-11");
        tradeLogFact.pushAddMoneyEvent(new TradeLogFact.AddMoneyEventFact(200000L, now.minusMinutes(30)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(200000L, now.minusMinutes(30)));
        tradeLogFact.pushSendMoneyEvent(new TradeLogFact.SendMoneyEventFact(199000L, now.minusMinutes(20)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(1000L, now.minusMinutes(20)));

        FraudDetectResultContent result = ruleEngine.runRuleCheck(userId, Optional.of(tradeLogFact));

        assertThat(result.getUserId(), equalTo(userId));
        assertThat(result.getIsFraud(), equalTo(true));
        assertThat(result.getRule(), containsString("RuleA"));
        assertThat(result.getRule(), not(containsString("RuleB")));
        assertThat(result.getRule(), not(containsString("RuleC")));
    }

    @Test
    public void runRuleCheck_RuleA_is_not_ok() {
        Long userId = 1L;
        LocalDateTime now = LocalDateTime.now();
        TradeLogFact tradeLogFact = new TradeLogFact();
        tradeLogFact.setUserId(userId);
        tradeLogFact.setAccountCreateTime(now.minusHours(2));
        tradeLogFact.setCreatedAccountNumber("11-11");
        tradeLogFact.pushAddMoneyEvent(new TradeLogFact.AddMoneyEventFact(200000L, now.minusHours(1).minusMinutes(30)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(200000L, now.minusHours(1).minusMinutes(30)));
        tradeLogFact.pushSendMoneyEvent(new TradeLogFact.SendMoneyEventFact(199000L, now.minusMinutes(20)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(1000L, now.minusMinutes(20)));

        FraudDetectResultContent result = ruleEngine.runRuleCheck(userId, Optional.of(tradeLogFact));

        assertThat(result.getUserId(), equalTo(userId));
        assertThat(result.getIsFraud(), equalTo(false));
        assertThat(result.getRule(), not(containsString("RuleA")));
        assertThat(result.getRule(), not(containsString("RuleB")));
        assertThat(result.getRule(), not(containsString("RuleC")));
    }

    @Test
    public void runRuleCheck_RuleB_is_ok() {
        Long userId = 1L;
        LocalDateTime now = LocalDateTime.now();
        String createdAccountNumber = "11-11";
        TradeLogFact tradeLogFact = new TradeLogFact();
        tradeLogFact.setUserId(userId);
        tradeLogFact.setAccountCreateTime(now.minusDays(7));
        tradeLogFact.setCreatedAccountNumber(createdAccountNumber);
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(50000L, createdAccountNumber, now.minusDays(5).minusMinutes(20)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(50000L, now.minusDays(5).minusMinutes(20)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(100000L, createdAccountNumber, now.minusDays(5).minusMinutes(19)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(150000L, now.minusDays(5).minusMinutes(19)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(50000L, createdAccountNumber, now.minusDays(4).minusMinutes(18)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(200000L, now.minusDays(4).minusMinutes(18)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(50000L, createdAccountNumber, now.minusDays(4).minusMinutes(18)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(250000L, now.minusDays(4).minusMinutes(18)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(100000L, createdAccountNumber, now.minusDays(3).minusMinutes(17)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(350000L, now.minusDays(3).minusMinutes(17)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(100000L, createdAccountNumber, now.minusDays(3).minusMinutes(16)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(450000L, now.minusDays(3).minusMinutes(16)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(100000L, createdAccountNumber, now.minusDays(2).minusMinutes(15)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(550000L, now.minusDays(2).minusMinutes(15)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(100000L, createdAccountNumber, now.minusDays(2).minusMinutes(14)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(650000L, now.minusDays(2).minusMinutes(14)));

        FraudDetectResultContent result = ruleEngine.runRuleCheck(userId, Optional.of(tradeLogFact));

        assertThat(result.getUserId(), equalTo(userId));
        assertThat(result.getIsFraud(), equalTo(true));
        assertThat(result.getRule(), not(containsString("RuleA")));
        assertThat(result.getRule(), containsString("RuleB"));
        assertThat(result.getRule(), not(containsString("RuleC")));
    }

    @Test
    public void runRuleCheck_RuleB_is_not_ok() {
        Long userId = 1L;
        LocalDateTime now = LocalDateTime.now();
        String createdAccountNumber = "11-11";
        TradeLogFact tradeLogFact = new TradeLogFact();
        tradeLogFact.setUserId(userId);
        tradeLogFact.setAccountCreateTime(now.minusDays(10));
        tradeLogFact.setCreatedAccountNumber(createdAccountNumber);
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(50000L, createdAccountNumber, now.minusDays(9).minusMinutes(20)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(50000L, now.minusDays(9).minusMinutes(20)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(100000L, createdAccountNumber, now.minusDays(9).minusMinutes(19)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(150000L, now.minusDays(9).minusMinutes(19)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(50000L, createdAccountNumber, now.minusDays(4).minusMinutes(18)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(200000L, now.minusDays(4).minusMinutes(18)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(50000L, createdAccountNumber, now.minusDays(4).minusMinutes(18)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(250000L, now.minusDays(4).minusMinutes(18)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(100000L, createdAccountNumber, now.minusDays(3).minusMinutes(17)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(350000L, now.minusDays(3).minusMinutes(17)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(100000L, createdAccountNumber, now.minusDays(3).minusMinutes(16)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(450000L, now.minusDays(3).minusMinutes(16)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(100000L, createdAccountNumber, now.minusDays(2).minusMinutes(15)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(550000L, now.minusDays(2).minusMinutes(15)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(100000L, createdAccountNumber, now.minusDays(2).minusMinutes(14)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(650000L, now.minusDays(2).minusMinutes(14)));

        FraudDetectResultContent result = ruleEngine.runRuleCheck(userId, Optional.of(tradeLogFact));

        assertThat(result.getUserId(), equalTo(userId));
        assertThat(result.getIsFraud(), equalTo(false));
        assertThat(result.getRule(), not(containsString("RuleA")));
        assertThat(result.getRule(), not(containsString("RuleB")));
        assertThat(result.getRule(), not(containsString("RuleC")));
    }

    @Test
    public void runRuleCheck_RuleC_is_ok() {
        Long userId = 1L;
        LocalDateTime now = LocalDateTime.now();
        String createdAccountNumber = "11-11";
        TradeLogFact tradeLogFact = new TradeLogFact();
        tradeLogFact.setUserId(userId);
        tradeLogFact.setAccountCreateTime(now.minusDays(10));
        tradeLogFact.setCreatedAccountNumber(createdAccountNumber);
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(50000L, createdAccountNumber, now.minusMinutes(20)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(50000L, now.minusMinutes(20)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(100000L, createdAccountNumber, now.minusMinutes(19)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(150000L, now.minusMinutes(19)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(50000L, createdAccountNumber, now.minusMinutes(18)));
        tradeLogFact.pushLastBalanceFact(new TradeLogFact.LastBalanceFact(200000L, now.minusMinutes(18)));

        FraudDetectResultContent result = ruleEngine.runRuleCheck(userId, Optional.of(tradeLogFact));

        assertThat(result.getUserId(), equalTo(userId));
        assertThat(result.getIsFraud(), equalTo(true));
        assertThat(result.getRule(), not(containsString("RuleA")));
        assertThat(result.getRule(), not(containsString("RuleB")));
        assertThat(result.getRule(), containsString("RuleC"));
    }

    @Test
    public void runRuleCheck_RuleC_is_not_ok() {
        Long userId = 1L;
        LocalDateTime now = LocalDateTime.now();
        String createdAccountNumber = "11-11";
        TradeLogFact tradeLogFact = new TradeLogFact();
        tradeLogFact.setUserId(userId);
        tradeLogFact.setAccountCreateTime(now.minusDays(10));
        tradeLogFact.setCreatedAccountNumber(createdAccountNumber);
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(50000L, createdAccountNumber, now.minusDays(5).minusMinutes(20)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(50000L, createdAccountNumber, now.minusDays(5).minusMinutes(20)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(100000L, createdAccountNumber, now.minusMinutes(19)));
        tradeLogFact.pushReceiveMoneyEvent(new TradeLogFact.ReceiveEventFact(50000L, createdAccountNumber, now.minusMinutes(18)));

        FraudDetectResultContent result = ruleEngine.runRuleCheck(userId, Optional.of(tradeLogFact));

        assertThat(result.getUserId(), equalTo(userId));
        assertThat(result.getIsFraud(), equalTo(false));
        assertThat(result.getRule(), not(containsString("RuleA")));
        assertThat(result.getRule(), not(containsString("RuleB")));
        assertThat(result.getRule(), not(containsString("RuleC")));
    }
}